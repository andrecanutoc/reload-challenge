# Reload Challenge

## Get Started

- Edit *App.js* to change this screen and then come back to see your edits.
- Pess Cmd + R in nthe simulator to reload your app's code.

## What i do?

Create a screen fallowing these instructions:

- [x] Create Header Component
- [x] Create Calendar Component;
- [x] Select date (go to the future and past dates);
- [x] The component should work inside a modal as the example;
- [x] The modal should be closed using swipe down;
- [x] The component must allow select range of dates and mark a line with linear gradient between the first and last date selected. (This is the most important requirement) (Cf. screenshot below)

## Styles

*Calendar Range Gradient:*
background-image: linear-gradient(261deg, #4a54df 83%, #15d4d8 11%);

*Button Apply Gradient:*
background-image: linear-gradient(43deg, #4a54df 4%, #15d4d8 69%);

Fonts:
- Roboto
- Roboto-Bold

Upload the source code on GitHub, Gitlab, Bitbucker or other.