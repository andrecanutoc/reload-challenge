/**
 * Reload Challenge React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';

import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
  Text,
  Button,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import SwipeUpDownModal from 'react-native-swipe-modal-up-down';

import TopHeader from './src/components/TopHeader';
import Section from './src/components/Section';
import CalendarCard from './src/components/Calendar';

const App = () => {
  let [ShowComment, setShowModelComment] = useState(false);
  let [animateModal, setanimateModal] = useState(false);

  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <TopHeader title="Teste" />
      <View
        style={{
          backgroundColor: isDarkMode ? Colors.black : Colors.white,
        }}>
        <Section
          title="Ação de marcação"
          children="Teste para a vaga de React Native para a Reload. Calendário personalizado."
        />
        <Button
          title={'Realizar agendamento'}
          style={styles.buttonAct}
          onPress={() => {
            setShowModelComment(true);
          }}
        />
      </View>
      <SwipeUpDownModal
        modalVisible={ShowComment}
        PressToanimate={animateModal}
        ContentModal={<CalendarCard />}
        ContentModalStyle={styles.Modal}
        onClose={() => {
          setanimateModal(false);
          setShowModelComment(false);
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  buttonAct: {
    marginTop: 40,
  },
  Modal: {
    marginTop: '100%',
    backgroundColor: '#fff',
  },
});

export default App;
