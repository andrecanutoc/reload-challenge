import React from 'react';
import {SvgXml} from 'react-native-svg';

const Icons = {
  Logo: `
    <svg viewBox="0 0 60 100">
        <defs>
            <linearGradient id="linear-gradient" x1="-.2" x2="1.856" y1="4.185" y2="-2.766" gradientUnits="objectBoundingBox">
                <stop offset="0" stop-color="#4a54df"/>
                <stop offset="1" stop-color="#15d4d8"/>
            </linearGradient>
            <linearGradient id="linear-gradient-2" x1="4.903" x2="1.996" y1="-13.009" y2="-3.368" xlink:href="#linear-gradient"/>        
        </defs>
        <g id="Grupo_60" data-name="Grupo 60" transform="translate(-393 -234.855)">
            <path id="XMLID_36_" fill="#4a54df" d="M207.2 52.687v-6.544a2.581 2.581 0 012.643-2.643H243.7a2.581 2.581 0 012.643 2.643v6.544a2.581 2.581 0 01-2.643 2.643h-33.857a2.658 2.658 0 01-2.643-2.643z" transform="translate(256.024 193.494)"/>
            <path id="XMLID_35_" fill="url(#linear-gradient)" d="M207.2 67.987v-6.544a2.581 2.581 0 012.643-2.643H243.7a2.581 2.581 0 012.643 2.643v6.544a2.581 2.581 0 01-2.643 2.643h-33.857a2.579 2.579 0 01-2.643-2.643z" transform="translate(256.024 197.449)"/>
            <path id="XMLID_34_" fill="url(#linear-gradient-2)" d="M207.2 83.387v-6.544a2.581 2.581 0 012.643-2.643H243.7a2.581 2.581 0 012.643 2.643v6.544a2.581 2.581 0 01-2.643 2.643h-33.857a2.658 2.658 0 01-2.643-2.643z" transform="translate(256.024 201.43)"/>
            <g id="XMLID_15_" transform="translate(393 234.855)">
                <path id="XMLID_27_" d="M180.975 94.224l-11.453-19.381h-9.061v19.381H151.4V42.5h22.779c10.194 0 16.99 6.67 16.99 16.235 0 9.187-6.041 14.221-12.333 15.228l12.71 20.387h-10.571zm.755-35.489c0-5.034-3.775-8.18-8.809-8.18h-12.46v16.486h12.459c5.034-.126 8.81-3.398 8.81-8.306z" fill="#525252" transform="translate(-151.4 -41.619)"/>
                <path id="XMLID_25_" d="M262.6 94.224V42.5h9.061v43.8h22.779v8.05H262.6z" fill="#525252" transform="translate(-122.656 -41.619)"/>
                <path id="XMLID_22_" d="M309.3 68.606c0-15.479 10.949-26.806 26.428-26.806s26.428 11.326 26.428 26.806-10.949 26.806-26.428 26.806S309.3 84.085 309.3 68.606zm43.67 0c0-10.7-6.8-18.752-17.116-18.752-10.445 0-17.116 8.054-17.116 18.752s6.67 18.752 17.116 18.752c10.32-.126 17.116-8.181 17.116-18.752z" fill="#525252" transform="translate(-110.584 -41.8)"/>
                <path id="XMLID_19_" d="M412.056 94.25l-3.776-9.942H384.5l-3.78 9.942H370.4l20.388-51.85h11.326L422.5 94.25zm-15.605-42.663l-9.313 24.666h18.626z" fill="#525252" transform="translate(-94.79 -41.645)"/>
                <path id="XMLID_16_" d="M434.8 94.224V42.5h19.255c16.109 0 27.057 10.7 27.057 25.925S470.164 94.35 454.055 94.35H434.8zm37-25.8c0-10.068-6.167-18-17.745-18h-10.194V86.3h10.194c11.201 0 17.745-8.185 17.745-17.875z" fill="#525252" transform="translate(-78.143 -41.619)"/>
            </g>
        </g>
    </svg>`,
  Chat: '<svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>',
};

export const SAOIcons = props => {
  const {iconName, width, height, color} = props;
  return (
    <SvgXml xml={Icons[iconName]} width={width} height={height} fill={color} />
  );
};
