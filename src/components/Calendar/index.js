import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import DateRangePicker from './DateRangePicker';

const CalendarCard = () => {
  return (
    <View style={styles.sectionContainer}>
      <View>
        <DateRangePicker
          initialRange={['2021-10-01', '2021-10-10']}
          onSuccess={(s, e) => console.log(s + ' até ' + e)}
        />
        <LinearGradient
          style={styles.gradientOverlay}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#4a54df', '#15d4d8']}
        />
      </View>
      <TouchableOpacity>
        <LinearGradient
          style={styles.btnDefault}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#4a54df', '#15d4d8']}>
          <Text style={styles.btnText}>Apply</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  btnDefault: {
    padding: 20,
    marginVertical: 20,
    borderRadius: 100,
  },
  btnText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  gradientOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    opacity: 0.25,
    zIndex: -1,
  },
});

export default CalendarCard;
