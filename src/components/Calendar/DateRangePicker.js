import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import { Calendar, defaultStyle } from 'react-native-calendars';

const XDate = require('xdate');

type Props = {
  initialRange: React.PropTypes.array.isRequired,
  onSuccess: React.PropTypes.func.isRequired,
};
export default class DateRangePicker extends Component<Props> {

  state = {isFromDatePicked: false, isToDatePicked: false, markedDates: {}}

  componentDidMount() { this.setupInitialRange() }

  onDayPress = (day) => {
    if (!this.state.isFromDatePicked || (this.state.isFromDatePicked && this.state.isToDatePicked)) {
      this.setupStartMarker(day)
    } else if (!this.state.isToDatePicked) {
      let markedDates = {...this.state.markedDates}
      let [mMarkedDates, range] = this.setupMarkedDates(this.state.fromDate, day.dateString, markedDates)
      if (range >= 0) {
        this.setState({isFromDatePicked: true, isToDatePicked: true, markedDates: mMarkedDates})
        this.props.onSuccess(this.state.fromDate, day.dateString)
      } else {
        this.setupStartMarker(day)
      }
    }
  }

  setupStartMarker = (day) => {
    let markedDates = {[day.dateString]: {startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}}
    this.setState({isFromDatePicked: true, isToDatePicked: false, fromDate: day.dateString, markedDates: markedDates})
  }

  setupMarkedDates = (fromDate, toDate, markedDates) => {
    let mFromDate = new XDate(fromDate)
    let mToDate = new XDate(toDate)
    let range = mFromDate.diffDays(mToDate)
    if (range >= 0) {
      if (range == 0) {
        markedDates = {[toDate]: {color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}}
      } else {
        for (var i = 1; i <= range; i++) {
          let tempDate = mFromDate.addDays(1).toString('yyyy-MM-dd')
          if (i < range) {
            markedDates[tempDate] = {color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
          } else {
            markedDates[tempDate] = {endingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
          }
        }
      }
    }
    return [markedDates, range]
  }

  setupInitialRange = () => {
    if (!this.props.initialRange) return
    let [fromDate, toDate] = this.props.initialRange
    let markedDates = {[fromDate]: {startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}}
    let [mMarkedDates, range] = this.setupMarkedDates(fromDate, toDate, markedDates)
    this.setState({markedDates: mMarkedDates, fromDate: fromDate})
  }

  render() {
    return (
      <Calendar {...this.props}
                markingType={'period'}
                current={this.state.fromDate}
                markedDates={this.state.markedDates}
                dayComponent={({date, state, marking, theme}) => {
                  return marking ? (
                    <TouchableOpacity onPress={() => {this.onDayPress(date)}}>
                      <Text style={styles.dayButton}>{date.day}</Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity onPress={() => {this.onDayPress(date)}}>
                      <Text
                        style={[
                          styles.dayButton,
                          {
                            color:
                              state === 'disabled'
                                ? 'gray'
                                : state === 'today'
                                ? '#4a54df'
                                : 'black',
                            backgroundColor: '#fff',
                          },
                        ]}>
                        {date.day}
                      </Text>
                    </TouchableOpacity>
                  );
                }}
                theme={{
                  calendarBackground: 'transparent',
                  markColor: 'red',
                  markTextColor: 'black',
                  'stylesheet.calendar.header': {
                    header: {
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      paddingLeft: 10,
                      paddingRight: 10,
                      marginTop: 0,
                      alignItems: 'center',
                      backgroundColor: '#fff',
                    },
                    week: {
                      paddingTop: 7,
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      backgroundColor: '#fff',
                    },
                    dayHeader: {
                      padding: 15,
                      backgroundColor: '#fff',
                    },
                  },
                  'stylesheet.calendar.main': {
                    container: {
                      padding: 0,
                    },
                    week: {
                      padding: 0,
                      margin: 0,
                      flexDirection: 'row',
                    },
                    dayContainer: {
                      flex: 1,
                      padding: 0,
                      alignItems: 'center',
                      backgroundColor: 'transparent',
                    },
                  },
                }}
                onDayPress={(day) => {this.onDayPress(day)}}/>
    )
  }
}

const styles = StyleSheet.create({
  dayButton: {
    fontSize: 16,
    textAlign: 'center',
    backgroundColor: 'transparent',
    padding: 10,
    width: 50,
    height: 40,
  },
});

DateRangePicker.defaultProps = {
  theme: { markColor: '#00adf5', markTextColor: '#ffffff' }
};
