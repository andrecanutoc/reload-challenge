import React from 'react';
import {StyleSheet, Image, View} from 'react-native';
import {SAOIcons} from '../SvgIcons';

const TopHeader = ({children, title}) => {
  return (
    <View style={styles.sectionContainer}>
      <View style={styles.column}>
        <Image
          source={{
            uri: 'https://i.pravatar.cc/200',
          }}
          style={styles.avatar}
        />
      </View>
      <View style={[styles.column, styles.logo]}>
        <SAOIcons iconName={'Logo'} width={200} height={24} />
      </View>
      <View style={styles.column}>
        <SAOIcons iconName={'Chat'} width={30} height={30} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 24,
    minHeight: 50,
  },
  column: {
    alignItems: 'center',
  },
  avatar: {
    flex: 1,
    width: 30,
    height: 30,
    borderRadius: 20,
  },
  logo: {
    flex: 1,
    alignItems: 'flex-start',
    marginVertical: 10,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
});

export default TopHeader;
